#include <TM1637.h>
#include <DHT.h>
#include <TM1638.h>
#include <DHT1.h>



/***************************************************************************************
 *
 *
 *                           Salvador Baca  
 *                    Bluetooth HC-05 Communication
 *                          TM1637 7 Segment LED
 *                        DHT Temperature Sensor
 *                            Water Sensor
 *                            07/09/2015
 *
 * 	                ______________________
 * 			|P1.0		     |
 *    Bluetooth ----->	|P1.1 RX	     |
 *    Bluetooth	----->	|P1.2 TX	     |
 *              	|P1.3		     |
 * 	Water	----->	|P1.4		 P1.7|<------ Relay WindowDown 
 * 	Display	<-----	|P1.5		 P1.6|------->LED
 *      Display	<-----	|2.0	         P2.5|------->Relay WindowUp
 * Temperaturei	----->	|2.1	         P2.4|------>Display
 * Temperatureo	----->  |2.2		 P2.3|------>Display
 * 			|                    |
 * 			---------------------|
 *
 * 			
 *
 ***********************************************************************************/



/**********************************************************************************************************
 *
 *  The program performs the following tasks:
 *  If the program receives a "1" it will turn on Green LED and send temperature inside and outside information
 *  It the program receives a "0" it will turn off Green LED
 *  If the program temperaure inside car(t_bits[]) higher than temperature outside(tt_bits[]); it will open window
 *  If the program temperaure inside car(t_bits[]) lower than temperature outside(tt_bits[]); it will close window
 *             
 ***************************************************************************************************************/
 
//MACRO Define Sensors 1
#define CLK  7                          //4-digital display clock pin yellow                
#define DIO  8                          //4-digital display data pin white
#define Water  6                        //Water Sensor Pin 
#define BLINK_LED  RED_LED              //Blink LED
#define TEMP_HUMI_PIN  9                //Pin of temperature&humidity sensor

//MACRO Define Sensors 2
#define CLK1  11                          //4-digital display clock pin                   
#define DIO1  12                          //4-digital display data pin
#define BLINK_LED 2                       //Blink LED
#define TEMP_PIN  10                      //Pin of temperature&humidity sensor

//Macro Define for Relays
#define WindowUp 13
#define WindowDown 15


/*Global Variables  Sensor 1*/
TM1637 TM1637(CLK, DIO);                //4-digital display object
DHT DHT(TEMP_HUMI_PIN, DHT22);          //Temperature&humidity sensor object
int8_t t_bits[2] = {0};                 //Array to store the single bits of the temperature
int8_t h_bits[2] = {0};                 //Array to store the single bits of the humidity

/*Global Variables Sensor 2*/
TM1638 TM1638(CLK1, DIO1);               //4-digital display object
DHTT DHTT(TEMP_PIN, DHT22);              //Temperature&humidity sensor object
int8_t tt_bits[2] = {0};                 //Array to store the single bits of the temperature
int8_t hh_bits[2] = {0};                 //Array to store the single bits of the humidity

/*Initialize Strings that will be sent to app*/
String temp = "";                       //Initialize String
String temp1 = "";                      //Initialize String
String tempi = "";                      //Initialize string temperature inside
String tempo = "";                      //Initialize string temperature outside

//Flags to keep track of actions

int state = 0;          //Store value received trough bluetooth
int flag = 0;           //Flag that indicates that feedback has been sent to terminal
int temp_hot =1;        //Flag to let us know that HOT signal was already sent
int temp_sent =0;       //Flag to let us know that Temp signal was already sent when opening window
int temp_sent2 =0;      //Flag to let us know that Temp signal was already sent when closing window
int WaterState =0;      //Flag to let us know if it is raining
int Water_flag =0;      //Flag to let us know that Water signal was already sent
int WindowStatus =1;    //Allows me to keep track of where window is


void setup()
{
  
  /*Setup for temperature Sensor 1*/
  TM1637.init();                  //Initialize 4-digital display
   TM1637.set(BRIGHT_TYPICAL);    //Temperature&humidity sensor object
   TM1637.point(POINT_ON);        //Light the Clock point ":"
  DHT.begin();                    //Initialize temperature and humidity sensor
  pinMode(RED_LED, OUTPUT);       //Declare the red_led pin as an OUTPUT     
  
  /*Setup for temperature Sensor 2*/

  TM1638.init();                  //Initialize 4-digital display
   TM1638.set(BRIGHT_TYPICAL);    //Temperature&humidity sensor object
   TM1638.point(POINT_ON);        //Light the Clock point ":"
  DHTT.begin();                    //Initialize temperature and humidity sensor
  pinMode(BLINK_LED, OUTPUT);       //Declare the red_led pin as an OUTPUT         
  
  /*Initialize Pinmodes*/
  pinMode(GREEN_LED, OUTPUT);      //Set Green LED as an output
  digitalWrite(GREEN_LED, LOW);    //Turn off Green LED
  pinMode(Water, INPUT);               //Pin 6 as an input  
  pinMode(WindowUp, OUTPUT);      //Set Window as an output
  pinMode(WindowDown, OUTPUT);      //Set Window pin as an output

  Serial1.begin(9600);            //Set baud rate at 9600
}



void loop() 
{
  /*Set up for temperature Sensor 1*/
  int _temperature= DHT.readTemperature();                    //Read the temperature value from temperature sensor
  int _humidity = DHT.readHumidity();                         //Read the humidity value from humidity sensor
  memset(t_bits, 0, 2);                                       //Reset Array once used
  memset(h_bits, 0, 2);
  t_bits[0] = _temperature % 10;                              //4-digital-display [0,1] is used to display temperature
  _temperature /= 10;
  t_bits[1] = _temperature % 10;                              //4-digital-display [2,3] is used to display temperature
  h_bits[0] = _humidity % 10;
  _humidity /= 10;
  h_bits[1] = _humidity % 10;
  
  
  /*Set up for temperature Sensor 2 */
  int _temperature1= DHTT.readTemperature();                    //Read the temperature value from temperature sensor
  int _humidity1 = DHTT.readHumidity();                         //Read the humidity value from humidity sensor
  memset(tt_bits, 0, 2);                                       //Reset Array once used
  memset(hh_bits, 0, 2);
  tt_bits[0] = _temperature1 % 10;                              //4-digital-display [0,1] is used to display temperature
  _temperature1 /= 10;
  tt_bits[1] = _temperature1 % 10;                              //4-digital-display [2,3] is used to display temperature
  hh_bits[0] = _humidity1 % 10;
  _humidity1 /= 10;
  hh_bits[1] = _humidity1 % 10;
  
  WaterState = digitalRead(Water);                                 //Read Pin 6 to check temperature sensor
  
  temp = String(t_bits[1]) + String(t_bits[0]) + "~";          //Concatenate into a single string to be sent to the app
  temp1 = String(tt_bits[1]) + String(tt_bits[0]) + "~";        //Concatenate into a single string to be sent to the app
  
    if(Serial1.available() > 0)
  {
    state = Serial1.read();         //store value received to state to later compare. 
    flag = 0;
  }
  if(state == '0')  /***When we want to close the window***/
  {

    digitalWrite(GREEN_LED, LOW);  //Turn off green LED
    if((temp_sent2 == 0)&&(WaterState == 1))
     {  
        Serial1.println("");        //Output on terminal   
        Serial1.print("x~");        //Output on terminal
        delay(500);                   //Delay .5 Sec    
        Serial1.println("");        //Output on terminal 
        tempi = String( "i" + temp);
        Serial1.print(tempi);        // Display temperature inside on app
    
        delay(500);                   //Delay .5 Sec    
        Serial1.println("");        //Output on terminal 
        tempo = String( "o" + temp1);//Temperature outside Sensor two
        Serial1.print(tempo);        // Display temperature inside on app
        delay(500);                   //Delay .5 Sec 
        
        
        if(WindowStatus == 2)//When window is fully open
        {
              /***********************Fully Close*************************************/
              /***********************Use 3.1 Sec Delay*******************************/

                    digitalWrite(WindowDown, HIGH); //Turn on green LED
                    digitalWrite(WindowUp, LOW); //Turn on green LED
                    delay(1000);                   //Delay 1 Sec
                    delay(1000);                   //Delay 1 Sec
                    delay(1000);                   //Delay 1 Sec
                    delay(100);                   //Delay 1 Sec
              
                    digitalWrite(WindowUp, HIGH); //Turn on green LED
            digitalWrite(WindowDown, HIGH); //Turn on green LED  
            digitalWrite(WindowUp, HIGH); //Turn on green LED  
                    delay(1000);                   //Delay 1 Sec
                    WindowStatus = 1;
             
              /************************************************************************/
    
        }
        else if(WindowStatus == 3)//When Window is Partially Open 
        {
            /**********Partially Close*************************************************/
            /********************** Use .38 Delay***************************************/

                  digitalWrite(WindowDown, HIGH); //Turn on green LED  
                  digitalWrite(WindowUp, LOW); //Turn on green LED
                  delay(380);                   //Delay 1 Sec
            
                  digitalWrite(WindowUp, HIGH); //Turn on green LED
                  delay(1000);                   //Delay 1 Sec
                  WindowStatus = 1;
            digitalWrite(WindowDown, HIGH); //Turn on green LED  
            digitalWrite(WindowUp, HIGH); //Turn on green LED  
            

            /************************************************************************/

        }
            

        temp_sent2 =1;             //Set Flag to let MSP know that temperature and signal was sent
     }
     
    temp_sent = 0;           //Re-initilialize flag for window up    
    
    if(flag == 0)
    {
      flag =1;
    }
    
  }

      else if(state =='1')/***Window Open***/
  {
     digitalWrite(GREEN_LED, HIGH); //Turn on green LED
     delay(1000);                   //Delay 1 Sec
     digitalWrite(GREEN_LED, LOW);  //Turn off green LED

     if((temp_sent == 0)&&(WaterState == 1))
     {

       Serial1.println("");        //Output on terminal 
       Serial1.print("h~");        //Output on terminal
       delay(500);                   //Delay .5 Sec
        
        Serial1.println("");          //Output on terminal 
        tempi = String( "i" + temp); //Concatenate i +temp bits + ~
        Serial1.print(tempi);         // Display temperature inside on app
    
        delay(500);                   //Delay .5 Sec    
        Serial1.println("");           //Output on terminal 
        tempo = String( "o" + temp1); //Temperature outside Sensor two
        Serial1.print(tempo);          // Display temperature inside on app
        
        if(WindowStatus == 1)//When Window is fully Closed 
        {
            /********************** Fully Close***************************************/ 
            /********************** Use 2.360 Delay***************************************/
  
                  digitalWrite(WindowUp, HIGH); //Turn on green LED
                  digitalWrite(WindowDown, LOW); //Turn on green LED
                  delay(1000);                   //Delay 1 Sec
                  delay(1000);                   //Delay 1 Sec
                  delay(360);                   //Delay 1 Sec
            
                  digitalWrite(WindowDown, HIGH); //Turn on green LED
                  delay(1000);                   //Delay 1 Sec
                  WindowStatus = 2;
                  digitalWrite(WindowDown, HIGH); //Turn on green LED  
                  digitalWrite(WindowUp, HIGH); //Turn on green LED  

            /************************************************************************/

                  
        }
        else if(WindowStatus ==3)
        {
          /********************** Open from partially open*****************************/ 
          /********************** Use 2.120Delay***************************************/
    
                digitalWrite(WindowUp, HIGH); //Make Sure Relay 2 is off
                digitalWrite(WindowDown, LOW); //Roll window Down
                delay(1000);                   //Delay 1 Sec
                delay(1000);                   //Delay 1 Sec
                delay(120);                   //Delay 1 Sec
          
                digitalWrite(WindowDown, HIGH); //Stop Rolling Window Down
                delay(1000);                   //Delay 1 Sec
                WindowStatus = 2;
                digitalWrite(WindowDown, HIGH); //Turn on green LED  
                digitalWrite(WindowUp, HIGH); //Turn on green LED  
                    
         /************************************************************************/
          
        
        }

         temp_sent =1;            //Set Flag to let MSP know that temperature and signal was sent
     }
     
    temp_sent2 = 0;        //Re-initilialize flag for window down       
    if(flag == 0)
    {     
      flag =1;
      
    }
  }  
  
  
  
  
  
  
 /**************************     *Code to monitor rain Sensor*           *******************/
/*****************************************************************************************************************/ 
  
  if((WaterState == 0) && (Water_flag == 0))              /***Check when water is sensed***/
  {
    //Serial1.println("Water!!!");  //Output on terminal 
    Serial1.println("");        //Output on terminal 
    Serial1.print("w~");        //Output on terminal
    delay(500);                   //Delay .5 Sec
    
        if(WindowStatus == 2)//When window is fully open
        {
              /***********************Fully Close*************************************/
              /***********************Use 3.1 Sec Delay*******************************/

                    digitalWrite(WindowDown, HIGH); //Turn on green LED
                    digitalWrite(WindowUp, LOW); //Turn on green LED
                    delay(1000);                   //Delay 1 Sec
                    delay(1000);                   //Delay 1 Sec
                    delay(1000);                   //Delay 1 Sec
                    delay(100);                   //Delay 1 Sec
              
                    digitalWrite(WindowUp, HIGH); //Turn on green LED
            digitalWrite(WindowDown, HIGH); //Turn on green LED  
            digitalWrite(WindowUp, HIGH); //Turn on green LED  
                    delay(1000);                   //Delay 1 Sec
                    WindowStatus = 1;
             
              /************************************************************************/
    
        }
        else if(WindowStatus == 3)//When Window is Partially Open 
        {
            /**********Partially Close*************************************************/
            /********************** Use .38 Delay***************************************/

                  digitalWrite(WindowDown, HIGH); //Turn on green LED  
                  digitalWrite(WindowUp, LOW); //Turn on green LED
                  delay(380);                   //Delay 1 Sec
            
                  digitalWrite(WindowUp, HIGH); //Turn on green LED
                  delay(1000);                   //Delay 1 Sec
                  WindowStatus = 1;
            digitalWrite(WindowDown, HIGH); //Turn on green LED  
            digitalWrite(WindowUp, HIGH); //Turn on green LED  
            

            /************************************************************************/

        }
        
    temp_sent2 =0;             //Set Flag to let MSP know that temperature and signal was sent
    Water_flag = 1;              //Water Flag to prevent sending too many signals
  }
  
  if((WaterState == 1) && (Water_flag == 1))            /***Check when it stops raining***/
  {
    Serial1.println("");        //Output on terminal 
    Serial1.print("d~");        //Output on terminal   
    delay(500);                   //Delay .5 Sec
    
    Water_flag = 0;              //Reset water Flag to prevent sending too many signals
  }
  
  /*****************************************************************************************************************/
  
  
  
  
  
  
  
/**************************     *Code to monitor temperature inside and outside car*           *******************/
/*****************************************************************************************************************/
  if((t_bits[1] > tt_bits[1]) &&(temp_hot ==0)&&(WaterState == 1))                /*** Temperature inside the car is hotter than outside temperature**/
  {
    //Serial1.println("HOT!!!");  //Output on terminal 
    Serial1.println("");        //Output on terminal 
    Serial1.print("b~");        //Output on terminal   
    delay(500);                   //Delay .5 Sec    
    Serial1.println("");        //Output on terminal 
    tempi = String( "i" + temp);
    Serial1.print(tempi);        // Display temperature inside on app

    delay(500);                   //Delay .5 Sec    
    Serial1.println("");        //Output on terminal 
    tempo = String( "o" + temp1);//Temperature outside Sensor two
    Serial1.print(tempo);        // Display temperature inside on app
    
    if(WindowStatus == 1)//When Window is fully closed 
    {
      /********************** Use .3 Delay***************************************/

      digitalWrite(WindowUp, HIGH); //Turn on green LED  
      digitalWrite(WindowDown, LOW); //Turn on green LED
      delay(300);                   //Delay 1 Sec

      digitalWrite(WindowDown, HIGH); //Turn on green LED
      delay(1000);                   //Delay 1 Sec
      
      digitalWrite(WindowDown, HIGH); //Turn on green LED  
      digitalWrite(WindowUp, HIGH); //Turn on green LED  

      /************************************************************************/      
    }
    if(WindowStatus == 2)//If Window is fully 
    {
      
      /***********************Fully Close*************************************/
      /***********************Use 3.1 Sec Delay*************************************/
     
            digitalWrite(WindowDown, HIGH); //Turn on green LED
            digitalWrite(WindowUp, LOW); //Turn on green LED
            delay(1000);                   //Delay 1 Sec
            delay(1000);                   //Delay 1 Sec
            delay(1000);                   //Delay 1 Sec
            delay(100);                   //Delay 1 Sec
      
            digitalWrite(WindowUp, HIGH); //Turn on green LED
            
            digitalWrite(WindowDown, HIGH); //Turn on green LED  
            digitalWrite(WindowUp, HIGH); //Turn on green LED  

      /************************************************************************/
      
      /************************Partially Open*************************************/
      /********************** Use .3 Delay***************************************/
            
            digitalWrite(WindowUp, HIGH); //Turn on green LED  
            digitalWrite(WindowDown, LOW); //Turn on green LED
            delay(300);                   //Delay 1 Sec
      
            digitalWrite(WindowDown, HIGH); //Turn on green LED
            delay(1000);                   //Delay 1 Sec
            digitalWrite(WindowDown, HIGH); //Turn on green LED  
            digitalWrite(WindowUp, HIGH); //Turn on green LED  
            
      /************************************************************************/

    }   
    WindowStatus = 3;
    temp_hot =1;               //Temperature Signal was sent
  }
  
  
  if((t_bits[1] <= tt_bits[1])&&(temp_hot ==1)&&(WaterState == 1))             /***Temperature inside the car is cooler than outside car***/
  {
    //Serial1.println("NICE!!!"); //Output on terminal 

    Serial1.println("");        //Output on terminal 
    Serial1.print("c~");        //Output on terminal   
    delay(500);                   //Delay .5 Sec
    
    Serial1.println("");        //Output on terminal 
    tempi = String( "i" + temp);
    Serial1.print(tempi);        // Display temperature inside on app

    delay(500);                   //Delay .5 Sec    
    Serial1.println("");        //Output on terminal 
    tempo = String( "o" + temp1);//Temperature outside Sensor two
    Serial1.print(tempo);        // Display temperature outside on app
    temp_hot =0;               //Temperature signal 
    
    
    if(WindowStatus == 2)//If Window is fully Open
    {
      /***************************Fully Close***************************************/
      /************************Use 3.1 Sec Delay************************************/
     
            digitalWrite(WindowDown, HIGH); //Turn on green LED
            digitalWrite(WindowUp, LOW); //Turn on green LED
            delay(1000);                   //Delay 1 Sec
            delay(1000);                   //Delay 1 Sec
            delay(1000);                   //Delay 1 Sec
            delay(100);                   //Delay 1 Sec
      
            digitalWrite(WindowUp, HIGH); //Turn on green LED
            delay(1000);                   //Delay 1 Sec
            
            digitalWrite(WindowDown, HIGH); //Turn on green LED  
            digitalWrite(WindowUp, HIGH); //Turn on green LED  
      
      /************************************************************************/
            
    
    }
        if(WindowStatus == 3)// If Window is partially open
    {
      /***************************Partially Close**********************************/
      /***************************Use .38 Delay************************************/
      
            digitalWrite(WindowDown, HIGH); //Turn on green LED  
            digitalWrite(WindowUp, LOW); //Turn on green LED
            delay(380);                   //Delay 1 Sec
      
            digitalWrite(WindowUp, HIGH); //Turn on green LED
            delay(1000);                   //Delay 1 Sec
            
            digitalWrite(WindowDown, HIGH); //Turn on green LED  
            digitalWrite(WindowUp, HIGH); //Turn on green LED  
      
      /************************************************************************/
   
    }
    
    WindowStatus = 1;
  }



/*****************************************************************************************************************/
  /* show it 7-Segment Display*/ 
  TM1637.display(1, t_bits[0]);
  TM1637.display(0, t_bits[1]);
  TM1637.display(3, h_bits[0]);
  TM1637.display(2, h_bits[1]);
  
    /* show it Display on 2nd 7-Segment Display*/ 
  TM1638.display(1, tt_bits[0]);
  TM1638.display(0, tt_bits[1]);
  TM1638.display(3, hh_bits[0]);
  TM1638.display(2, hh_bits[1]);

}
  
