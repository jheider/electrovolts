package com.jessica.windows;

        import android.os.Bundle;

        import java.io.IOException;
        import java.io.InputStream;
        import java.io.OutputStream;
        import java.util.UUID;

        import android.app.Activity;
        import android.bluetooth.BluetoothAdapter;
        import android.bluetooth.BluetoothDevice;
        import android.bluetooth.BluetoothSocket;
        import android.content.Intent;

        import android.os.Handler;
        import android.view.View;
        import android.view.View.OnClickListener;
        import android.widget.TextView;
        import android.widget.Toast;
        import android.widget.ToggleButton;


public class MainActivity extends Activity
{

    ToggleButton toggleButton;
    TextView windowView0, tempView0, tempView1, rain;
    Handler bluetoothIn;

    final int handlerState = 0;                        //used to identify handler message
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private StringBuilder recDataString = new StringBuilder();

    private ConnectedThread mConnectedThread;


    // SPP UUID service - this should work for most devices
    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    // String for MAC address
    private static String address;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        //Link the buttons and textViews to respective views
        toggleButton = (ToggleButton) findViewById(R.id.toggleButton);
        windowView0 = (TextView) findViewById(R.id.windowView0);
        tempView0 = (TextView) findViewById(R.id.tempView0);
        tempView1 = (TextView) findViewById(R.id.tempView1);
        rain = (TextView) findViewById(R.id.rain);

        bluetoothIn = new Handler()
        {
            public void handleMessage(android.os.Message msg)
            {
                if (msg.what == handlerState)                                //if message is what we want
                {
                    String readMessage = (String) msg.obj;                   // msg.arg1 = bytes from connect thread
                    recDataString.append(readMessage);                      //keep appending to string until ~

                    int endOfLineIndex = recDataString.indexOf("~");         // determine the end-of-line


                    if (endOfLineIndex > 0)
                    {                                           // make sure there is data before ~

                        String dataInPrint = recDataString.substring(3, endOfLineIndex);  //extract string that is received

                        int dataLength = dataInPrint.length();                          //get length of data received

                        //cases depending on what is sent through Bluetooth

                       if (recDataString.charAt(2) == 'i')      //receive 'i' for inside temperature, format: i##
                        {
                           tempView0.setText("Inside Temperature = " + dataInPrint + "C");
                        }
                        else if (recDataString.charAt(2) == 'o')    //receive 'o' for outside temperature, format: o##
                        {
                            tempView1.setText("Outside Temperature = " + dataInPrint + "C");
                        }
                        else if (recDataString.charAt(2) == 'x')     //receive 'x' when window is closed
                        {
                            windowView0.setText("Window is closed.");
                            Toast.makeText(getBaseContext(), "Windows rolled up.",Toast.LENGTH_SHORT).show();
                        }
                        else if (recDataString.charAt(2) == 'h') {     // receive 'h' when window is opened
                            windowView0.setText(" Window is open.");
                            Toast.makeText(getBaseContext(), "Windows rolled down.",Toast.LENGTH_SHORT).show();

                        }
                        else if (recDataString.charAt(2) == 'w')    //receive 'w' when it is raining
                        {
                            rain.setText("It is raining.");
                            toggleButton.setChecked(false);
                            windowView0.setText("Window is closed.");
                            Toast.makeText(getBaseContext(), "Windows rolled up.",Toast.LENGTH_SHORT).show();
                        }
                        else if (recDataString.charAt(2) == 'd')    //receive 'd' when it is not raining
                        {
                            rain.setText("It is not raining.");
                        }
                        else if (recDataString.charAt(2) == 'b')    //receive 'b' when outside temp is greater than inside
                        {
                            Toast.makeText(getBaseContext(), "It's getting hot!", Toast.LENGTH_SHORT).show();
                        }
                        else if (recDataString.charAt(2) == 'c')    //receive 'c' when inside temp is less than outside
                        {
                            Toast.makeText(getBaseContext(), "It's cooling down!", Toast.LENGTH_SHORT).show();
                        }
                        //otherwise do nothing

                        recDataString.delete(0, recDataString.length());                    //clear all string data

                    }
                }
            }
        };

        btAdapter = BluetoothAdapter.getDefaultAdapter();       // get Bluetooth adapter
        checkBTState();
    }


    public void toggleWindows(View view)
    {
        boolean on = ((ToggleButton) view).isChecked();

        if (on)
        {
            mConnectedThread.write("1"); //send "1" via Bluetooth to roll down windows
        }
        else
        {
            mConnectedThread.write("0"); //send "0" via Bluetooth to open windows
        }
    }

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException
    {
        return  device.createRfcommSocketToServiceRecord(BTMODULEUUID);  //creates secure outgoing connecetion with BT device using UUID
    }

    @Override
    public void onResume()
    {
        super.onResume();

        address = "20:15:05:20:18:01";             //MAC address for Bluetooth module

        //create device and set the MAC address
        BluetoothDevice device = btAdapter.getRemoteDevice(address);

        try
        {
            btSocket = createBluetoothSocket(device);
        } catch (IOException e) {
            Toast.makeText(getBaseContext(), "Socket creation failed", Toast.LENGTH_LONG).show();
        }
        // Establish the Bluetooth socket connection.
        try
        {
            btSocket.connect();
        } catch (IOException e)
        {
            try
            {
                btSocket.close();
            } catch (IOException e2)
            {
                //insert code to deal with this
            }
        }
        mConnectedThread = new ConnectedThread(btSocket);
        mConnectedThread.start();

        //I send a character when resuming.beginning transmission to check device is connected
        //If it is not an exception will be thrown in the write method and finish() will be called
        mConnectedThread.write("x");

    }

    @Override
    public void onPause()
    {
        super.onPause();

        try
        {
            //Don't leave Bluetooth sockets open when leaving activity
            btSocket.close();
        } catch (IOException e2) {
            //insert code to deal with this
        }

    }

    //Checks that the Android device Bluetooth is available and prompts to be turned on if off
    private void checkBTState() {

        if(btAdapter==null) {
            Toast.makeText(getBaseContext(), "Device does not support bluetooth", Toast.LENGTH_LONG).show();
        } else {
            if (btAdapter.isEnabled())
            {int x=0;
                x++;
            }
            else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }
    }

    //create new class for connect thread
    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        //creation of the connect thread
        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                //Create I/O streams for connection
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[256];
            int bytes;

            // Keep looping to listen for received messages
            while (true) {
                try {
                    bytes = mmInStream.read(buffer);            //read bytes from input buffer
                    String readMessage = new String(buffer, 0, bytes);
                    // Send the obtained bytes to the UI Activity via handler
                    bluetoothIn.obtainMessage(handlerState, bytes, -1, readMessage).sendToTarget();
                } catch (IOException e) {
                    break;
                }
            }

        }
        //write method
        public void write(String input) {
            byte[] msgBuffer = input.getBytes();           //converts entered String into bytes
            try {
                mmOutStream.write(msgBuffer);                //write bytes over BT connection via outstream
            } catch (IOException e) {
                //if you cannot write, close the application
                Toast.makeText(getBaseContext(), "Connection Failure", Toast.LENGTH_LONG).show();
                finish();

            }
        }
    }
}